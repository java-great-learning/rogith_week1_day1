package com.basics;
import java.util.Scanner;
import java.lang.*;
import java.util.Random;

public class SwitchCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int value1 = sc.nextInt();
		
		switch(value1)
		{
		case 1:
			double x1= sc.nextInt();
			System.out.println("Math.log("+x1+")="+ Math.log(x1));
			break;
		case 2:
			int x2 = sc.nextInt();
			int val = Math.abs(x2);
			System.out.println(val);
			break;
		case 3:
			int x3 = sc.nextInt();
			System.out.println(Math.sqrt(x3));
			break;
		case 4:
			int min=1, max=100;
			Random randomNumber = new Random();
			int res = min+randomNumber.nextInt(max);
			System.out.println(res);
			break;
		}
		
	}

}
