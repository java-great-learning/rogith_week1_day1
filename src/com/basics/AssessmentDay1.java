package com.basics;
import java.util.Scanner;
public class AssessmentDay1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//First Program in Java
		
		System.out.println("My First Program in Java");
		
		
		Scanner sc = new Scanner(System.in);
		
		int z; // Odd or Even --- If-Else
		z = sc.nextInt();
		if(z%2 == 0)
		{
			System.out.println("Even");
		}
		else
		{
			System.out.println("Odd");
		}
		
		
		int a; // Odd or even --- Ternary Operator
		a = sc.nextInt();
		String res = (a%2==0) ? "Even" : "Odd";
		System.out.println(res);

		
		System.out.println("Enter Year");
		int year;
		year = sc.nextInt();
		boolean flag = false;
		if(year%400 == 0)
		{
			flag = true;
		}
		else if(year%100 == 0)
		{
			flag = false;
		}
		else if(year%4 == 0)
		{
			flag = true;
		}
		else
		{
			flag = false;
		}

		if(flag)
		{
			System.out.println("Leap Year");
		}
		else
		{
			System.out.println("Not a Leap Year");
		}
		
	}

}
